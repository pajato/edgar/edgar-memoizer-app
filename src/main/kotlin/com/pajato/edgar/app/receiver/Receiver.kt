package com.pajato.edgar.app.receiver

internal const val ExitMessage = "Exiting Memoizer with code: "

/**
 * Start listening for commands to execute.
 *
 * @param args An optional argument specifying the file system location for the configuration directory.
 *
 * Exit codes:
 * | Value | Description
 * |=======|=============================================================================================
 * |  0    | Normal exit (received an "exit" command.
 * |  1    | LockFailAlreadyRunningException: an attempt was made to start a second memoizer app process.
 * |  2    | LockFailChannelClosedException: the channel was closed unexpectedly.
 * |  3    | LockFailIOErrorException: some unspecified IO error occurred.
 * |  4    | LockFailOverlappingException: an attempt was made to lock an overlapping region.
 * |  5    | LockFailUnexpectedException: an unexpected exception was encountered. See the log for details.
 * |  6    | InvalidDirectoryException: the configuration directory is not a directory.
 * |  7    | InvalidCreateException: the configuration directory could not be created.
 */
fun main(vararg args: String): Unit = try { runAppMaybe(args = args) } catch (exc: AppException) { exitWithCode(exc) }
