package com.pajato.edgar.app.receiver

import com.pajato.edgar.logger.Log
import com.pajato.edgar.memoizer.core.Config
import java.io.File
import java.nio.channels.FileChannel
import java.nio.file.Path
import kotlin.io.path.createDirectories

internal fun runAppMaybe(channel: FileChannel = Config.pidFile.channel, vararg args: String) {
    fun init(vararg args: String) { if (args.isNotEmpty()) { validateArgument(File(args[0])) } }

    init(*args)
    runMemoizer(channel)
    Log.add("${ExitMessage}0")
}

internal fun validateArgument(dir: File): Unit = when {
    dir.exists() && !dir.isDirectory -> throw DirectoryArgumentIsFileException()
    !dir.exists() -> Config.configBaseDir = createDirectory(dir.toPath())
    else -> Config.configBaseDir = dir
}

internal fun createDirectory(dir: Path): File = try { dir.createDirectories().toFile() } catch (exc: Exception) {
    throw DirectoryArgumentCreateException(exc.message)
}
