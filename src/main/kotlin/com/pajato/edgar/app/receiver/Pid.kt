package com.pajato.edgar.app.receiver

internal interface Pid {
    fun getPid(): Long
}
