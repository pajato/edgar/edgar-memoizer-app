package com.pajato.edgar.app.receiver

internal sealed class AppException(message: String? = null) : Exception(message)

internal class DirectoryArgumentIsFileException : AppException()
internal class DirectoryArgumentCreateException(message: String?) : AppException(message)
internal class LockFailAlreadyRunningException(message: String) : AppException(message)
internal class LockFailChannelClosedException : AppException()
internal class LockFailIOErrorException : AppException()
internal class LockFailOverlappingException : AppException()
internal class LockFailUnexpectedException : AppException()
internal class ExecuteFailedException(message: String?) : AppException(message)
